package it.baldarn.theparser;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.HashMap;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
@TestMethodOrder(MethodOrderer.Random.class)
class AppTest {

    @Test
    void testParseRequestLine() {
        var splittedLine = "123123123;10;200;10.10.10.10".split(";");
        var res = App.parseRequestLine(new HashMap<>(), splittedLine);

        var report = new Report("10.10.10.10").setNumberOfRequests(1).setTotalBytesSent(10);
        Assertions.assertEquals(res.get("10.10.10.10"), report);
    }

    @Test
    void testParse2RequestLine() {
        var splittedLine = "123123123;10;200;10.10.10.10".split(";");
        var res = App.parseRequestLine(App.parseRequestLine(new HashMap<>(), splittedLine), splittedLine);

        var report = new Report("10.10.10.10").setNumberOfRequests(2).setTotalBytesSent(20);
        Assertions.assertEquals(res.get("10.10.10.10"), report);
    }

    @Test
    void testParseRequestLineWithNoGoodRequest() {
        var input = Arrays.asList("123123123;10;200;10.10.10.10","123123123;10;200;10.10.10.10");
        var res = App.generateReportList(input.stream());

        Assertions.assertEquals(1, res.size());
    }

    // ... and so on...
}
