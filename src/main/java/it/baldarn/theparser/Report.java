package it.baldarn.theparser;

import java.util.Objects;

public class Report {

    private String ip;
    private Integer numberOfRequests = 0;
    private Double percentageOfTotalAmountOfRequests;
    private Integer totalBytesSent = 0;
    private Double percentageOfTotalAmountOfBytes;

    public Report(String ip) {
        this.ip = ip;
    }

    public String getIp() {
        return this.ip;
    }

    public Integer getNumberOfRequests() {
        return numberOfRequests;
    }

    public Report setNumberOfRequests(Integer numberOfRequests) {
        this.numberOfRequests = numberOfRequests;
        return this;
    }

    public Double getPercentageOfTotalAmountOfRequests() {
        return percentageOfTotalAmountOfRequests;
    }

    public Report setPercentageOfTotalAmountOfRequests(Double percentageOfTotalAmountOfRequests) {
        this.percentageOfTotalAmountOfRequests = percentageOfTotalAmountOfRequests;
        return this;
    }

    public Integer getTotalBytesSent() {
        return totalBytesSent;
    }

    public Report setTotalBytesSent(Integer totalBytesSent) {
        this.totalBytesSent = totalBytesSent;
        return this;
    }

    public Double getPercentageOfTotalAmountOfBytes() {
        return percentageOfTotalAmountOfBytes;
    }

    public Report setPercentageOfTotalAmountOfBytes(Double percentageOfTotalAmountOfBytes) {
        this.percentageOfTotalAmountOfBytes = percentageOfTotalAmountOfBytes;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Report report = (Report) o;
        return Objects.equals(ip, report.ip) && Objects.equals(numberOfRequests, report.numberOfRequests) && Objects.equals(percentageOfTotalAmountOfRequests, report.percentageOfTotalAmountOfRequests) && Objects.equals(totalBytesSent, report.totalBytesSent) && Objects.equals(percentageOfTotalAmountOfBytes, report.percentageOfTotalAmountOfBytes);
    }

    @Override
    public int hashCode() {
        return Objects.hash(ip, numberOfRequests, percentageOfTotalAmountOfRequests, totalBytesSent, percentageOfTotalAmountOfBytes);
    }

    @Override
    public String toString() {
        return ip + "," +
            numberOfRequests + "," +
            percentageOfTotalAmountOfRequests + "," +
            totalBytesSent + "," +
            percentageOfTotalAmountOfBytes + System.lineSeparator();
    }
}
