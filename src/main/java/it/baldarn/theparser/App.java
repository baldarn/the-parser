package it.baldarn.theparser;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class App {

    /**
     * As software developer, you have to implement a method in order to produce the following daily report:
     * /reports/ipaddr.csv: it must be a text/csv file containing the traffic data per IP Address.
     * Each rows must have the following fields :
     * <p>
     * IP Address
     * Number of requests
     * Percentage of the total amount of requests
     * Total Bytes sent
     * Percentage of the total amount of bytes
     * <p>
     * The data set must be sorted by the number of requests (DESC). The source data for your report is stored in the file
     * /logfiles/requests.log where each row (record) contains the following semicolon-separated values:
     * <p>
     * TIMESTAMP: the moment when the event occurred.
     * BYTES: the number of bytes sent to a client.
     * STATUS: HTTP response status.
     * REMOTE_ADDR: IP address of the client.
     * <p>
     * Exclude from your report all the lines in the source file where the STATUS is different from “OK” ( RFC 2616).
     * Note:
     * <p>
     * Write the requested code in Java;
     * Don’t use any frameworks but only the Java standard edition or at most, use light libraries;
     * It's not required, but allowing to choose between two possible output modes on daily report file (for example:
     * csv, json) will be taken into account;
     * It's not required, but adding unit tests will be taken into account.
     */
    private static final Path outputPath = Paths.get("output/reports/ipaddr.csv");

    public static void main(String[] args) {
        try (Stream<String> lines = Files.lines(Paths.get("input/logfiles/requests.log"))) {

            var reportList = generateReportList(lines);

            Files.delete(outputPath);
            reportList.forEach(report -> {
                try {
                    Files.write(outputPath, report.toString().getBytes(), StandardOpenOption.APPEND, StandardOpenOption.CREATE);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    protected static List<Report> generateReportList(Stream<String> lines) {
        var linesList = lines.collect(Collectors.toList());
        var totalBytes = linesList.stream().mapToInt(request -> Integer.parseInt(request.split(";")[1])).sum();
        var totalRequests = linesList.size();

        return linesList.stream()
                .map(line -> line.split(";"))
                .filter(splittedLine -> checkGoodRequest(splittedLine[2]))
                .reduce(
                        new HashMap<String, Report>(),
                        App::parseRequestLine,
                        (h1, h2) -> {
                            h1.putAll(h2);
                            return h1;
                        })
                .values()
                .stream()
                .map(report -> {
                    report.setPercentageOfTotalAmountOfBytes(report.getTotalBytesSent() / (double) totalBytes);
                    report.setPercentageOfTotalAmountOfRequests(report.getNumberOfRequests() / (double) totalRequests);
                    return report;
                })
                .collect(Collectors.toList());
    }

    protected static HashMap<String, Report> parseRequestLine(HashMap<String, Report> hashMap, String[] splittedLine) {
        var ipAddress = splittedLine[3];
        var bytes = Integer.valueOf(splittedLine[1]);

        var report = Optional
                .ofNullable(hashMap.get(ipAddress))
                .orElse(new Report(ipAddress));

        report.setNumberOfRequests(report.getNumberOfRequests() + 1);
        report.setTotalBytesSent(report.getTotalBytesSent() + bytes);
        hashMap.put(ipAddress, report);

        return hashMap;
    }

    protected static boolean checkGoodRequest(String htmlCode) {
        return htmlCode.equals("200");
    }
}
